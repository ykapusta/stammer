﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Stammer
{
    static class CONFIGURATION //константы путей к файлам словарей для окончаний, суффиксов, приставок
    {
        private static string suffixes_path = "suffixes";

        public static string SUFFIXES_PATH
        {
            get { return suffixes_path; }
            set { suffixes_path = value; }
        }

        private static string endings_path = "endings";

        public static string ENDINGS_PATH
        {
            get { return endings_path; }
            set { endings_path = value; }
        }

        private static string prefixes_path = "prefixes";

        public static string PREFIXES_PATH
        {
            get { return prefixes_path; }
            set { prefixes_path = value; }
        }

        public static int SUFFIX_DIVIDER = 2
            ; //влияет на потенциальное количество суффиксов, чем меньше - тем больше суффиксов может найти, но и шанс отрезать часть корня выше
    }

    public class Stammer
    {
        public string Prefix { get; set; }
        public string Root { get; set; }
        public string Suffix { get; set; }
        public string Ending { get; set; }
        public string Raw { get; set; }
        public bool Flag { get; set; }
        public bool Complete { get; set; }

        public Stammer(string word)
        {
            Prefix = "";
            Root = "";
            Suffix = "";
            Ending = "";
            Flag = false;
            Complete = false;
            Raw = word.ToLower();
            if (Raw != "")
            {
                Stam(word.ToLower());
                Detector();
                Complete = true;
            }
        }

        public Stammer(string raw, string prefix, string root, string suffix, string ending)
        {
            Prefix = prefix;
            Root = root;
            Suffix = suffix;
            Ending = ending;
            Raw = raw;
            Flag = false;
            Complete = true;
            Detector();
        }

        public override string ToString()
        {
            return string.Format(
                "Статус: {6}\n\n\nИсходное слово: {4}\n\n ======Разбор слова======\nПриставка: {0}\nКорень: {1}\nСуффикс(ы): {2}\nОкончание: {3}\n\n\nЯвляется наречием: {5}",
                Prefix, Root, Suffix, Ending, Raw, Flag ? "да" : "нет", Complete ? "выполнено" : "не выполнено");
        }

        string getDataAndJoin(string filename)
        {
            List<string> list = File.ReadAllLines(filename).ToList();
            list.Sort((x, y) => x.Length > y.Length ? -1 : 1);
            string forRegex = "";
            list.ForEach(e => { forRegex += "|" + e; });
            return forRegex.Substring(1);
        }

        private void Stam(string word)
        {
            Regex r = new Regex(@"");
            int z = Raw.Length - 2;
            if (z < 4)
            {
                #region engings

                r = new Regex("(" + getDataAndJoin(CONFIGURATION.ENDINGS_PATH) + "){0,1}$");
                var endgindMatches = r.Matches(word);
                if (endgindMatches.Count > 0) Ending = endgindMatches[0].ToString();
                word = word.Substring(0, word.Length - Ending.Length);

                #endregion

                #region prefixes

                r = new Regex("^(" + getDataAndJoin(CONFIGURATION.PREFIXES_PATH) + ")");
                var prefixMatches = r.Matches(word);
                if (prefixMatches.Count > 0) Prefix = prefixMatches[0].ToString();
                Root = Prefix.Length > 0 ? word.Substring(Prefix.Length) : word;

                #endregion
            }
            else
            {
                #region engings

                r = new Regex("(" + getDataAndJoin(CONFIGURATION.ENDINGS_PATH) + "){0,1}$");
                var endgindMatches = r.Matches(word);
                if (endgindMatches.Count > 0) Ending = endgindMatches[0].ToString();
                word = word.Substring(0, word.Length - Ending.Length);

                #endregion

                #region suffixes

                if (z > 0)
                {
                    Random rand = new Random();

                    #region suffixes

                    r = new Regex("(" + getDataAndJoin(CONFIGURATION.SUFFIXES_PATH) + "){0," + rand.Next(0, 4) + "}$");
                    var suffixesMatches = r.Matches(word).Cast<Match>().Select(match => match.Value).ToList();
                    if (suffixesMatches.Count > 0) Suffix = suffixesMatches[0];
                    word = word.Substring(0, word.Length - Suffix.Length);

                    #endregion
                }
                else
                {
                    Suffix = "";
                }

                #endregion

                #region prefixes

                r = new Regex("^(" + getDataAndJoin(CONFIGURATION.PREFIXES_PATH) + ")");
                var prefixMatches = r.Matches(word);
                if (prefixMatches.Count > 0) Prefix = prefixMatches[0].ToString();
                Root = Prefix.Length > 0 ? word.Substring(Prefix.Length) : word;

                #endregion
            }
        }

        private void Detector()
        {
            Flag = false;
            if (Suffix.Length > 0)
                if (Suffix.EndsWith("чь") || Suffix.EndsWith("шь") || Suffix.EndsWith("ее") || Suffix.EndsWith("ей") ||
                    Suffix.EndsWith("ше") || Suffix.EndsWith("е") ||
                    ((Prefix.StartsWith("по") || Prefix.StartsWith("со") || Prefix.StartsWith("с")) &&
                     Suffix.EndsWith("у")) ||
                    ((Prefix.StartsWith("до") || Prefix.StartsWith("из") || Prefix.StartsWith("ис") ||
                      Prefix.StartsWith("со") || Prefix.StartsWith("с")) && Suffix.EndsWith("а")) ||
                    ((Prefix.StartsWith("в") || Prefix.StartsWith("на")) && Suffix.EndsWith("е")) ||
                    ((Suffix.EndsWith("е") || Suffix.EndsWith("о")) &&
                     (Root.EndsWith("ш") || Root.EndsWith("щ") || Root.EndsWith("ц")) || Suffix.EndsWith("ше") ||
                     Suffix.EndsWith("ще") || Suffix.EndsWith("це")) ||
                    ((Prefix.StartsWith("в") || Prefix.StartsWith("на") || Prefix.StartsWith("за")) &&
                     Suffix.EndsWith("о")))
                {
                    Flag = true;
                }
        }
    }

    public static class StammerDictionary
    {
        static List<Stammer> dictionary = new List<Stammer>();

        public static void Initialize(string path = "words.txt")
        {
            string content = File.ReadAllText(path);
            Regex r = new Regex(@"---");
            Regex r2 = new Regex(@"(:\s[А-яЁё\s]+)\s");
            var array = r.Split(content);
            foreach (var s in array)
            {
                var spl = r2.Matches(s);
                dictionary.Add(
                    new Stammer(spl[0].Groups[0].ToString().Replace('\n', '\0'),
                        spl[1].Groups[0].ToString().Replace('\n', '\0'),
                        spl[2].Groups[0].ToString().Replace('\n', '\0'),
                        spl[3].Groups[0].ToString().Replace('\n', '\0'),
                        spl[4].Groups[0].ToString().Replace('\n', '\0')
                    )
                );
            }
            Console.WriteLine(dictionary.Count);
        }

        public static Stammer GetStammerFor(string word)
        {
            return dictionary.Find(d => d.Raw.Contains(word)) ?? new Stammer(word);
        }
    }
}