﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stammer
{
    public partial class Form1 : Form
    {
        Regex r = new Regex("[А-яЁё]+");
        Regex r2 = new Regex("([А-яЁё]+)");
        public Form1()
        {
            InitializeComponent();
        }

        private void analysis(object sender, EventArgs e)
        {
            resultTextBox.Text = Extract(wordTextBox.Text).ToString();
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Текст|*.txt";
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog.FileName;
            string fileText = File.ReadAllText(filename);
            var matches = r2.Matches(fileText);
            int counter = 0;
            if (matches.Count > 0)
            {
                string result = "";
                foreach (var m in matches)
                {
                    var tmp = Extract(m.ToString());
                        counter += tmp.Flag ? 1 : 0;
                        result += tmp.ToString() + "\n";
                }
                result += "\n\nКоличество наречий: " + counter;
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "Текст|*.txt";
                string resultPath = "";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    resultPath = dialog.FileName.EndsWith(".txt") ? dialog.FileName : dialog.FileName + ".txt";
                    File.WriteAllText(resultPath, result);
                    MessageBox.Show("Разбор выполнен. Результат сохранен в файл \"" + resultPath + "\".");
                    if (File.Exists(resultPath))
                    {
                        try { System.Diagnostics.Process.Start(resultPath); }
                        catch { }
                    }
                }
            }
            else
            {
                MessageBox.Show("В файле ненайдены подходящие слова.");
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Stammer v0.1");
        }

        private void wordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                resultTextBox.Text =  Extract(wordTextBox.Text).ToString();
            }
            else
            {
                resultTextBox.Text = "";
                var str = "" + e.KeyChar;
                if (!r.IsMatch(str) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Enter)
                {
                    e.KeyChar = '\0';
                }
            }
        }

        private Stammer Extract(string word="")
        {
            return StammerDictionary.GetStammerFor(word);
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
